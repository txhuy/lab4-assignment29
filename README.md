##Requirement: 
- Eclipse IDE
- Tomcat server 9
- mySql 8

##Starting guide
- Run sql/initDB.sql in mySql workbench to create DB and insert data.
- Edit user and password of mySql server in src/main/java/com/constructionmachinery/dao/impl/BaseDAO.java