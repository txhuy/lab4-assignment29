CREATE DATABASE computercourses DEFAULT CHARACTER SET utf8mb4;
USE computercourses;

CREATE TABLE courses (
	id bigint NOT NULL PRIMARY KEY auto_increment,
    title VARCHAR(255) NOT NULL,
    short_description TEXT NULL,
    long_description TEXT NULL,
    image VARCHAR(255) NULL,
    course_date TIMESTAMP NULL,
    created_date TIMESTAMP NULL,
    modified_date TIMESTAMP NULL,
    created_by VARCHAR(255) NULL,
    modified_by VARCHAR(255) NULL
);

CREATE TABLE introduction (
	id bigint NOT NULL PRIMARY KEY auto_increment,
    title VARCHAR(255) NOT NULL,
    description TEXT NULL,
    image VARCHAR(255) NULL,
    created_date TIMESTAMP NULL,
    modified_date TIMESTAMP NULL,
    created_by VARCHAR(255) NULL,
    modified_by VARCHAR(255) NULL
);

CREATE TABLE contact (
	id bigint NOT NULL PRIMARY KEY auto_increment,
    sitename VARCHAR(255) NOT NULL,
    short_description VARCHAR(255) NOT NULL,
    address VARCHAR(255) NULL,
    city VARCHAR(255) NULL,
    country VARCHAR(255) NULL,
    tel VARCHAR(50) NULL,
    email VARCHAR(255) NULL,
    created_date TIMESTAMP NULL,
    modified_date TIMESTAMP NULL,
    created_by VARCHAR(255) NULL,
    modified_by VARCHAR(255) NULL
);

CREATE TABLE feedback (
	id bigint NOT NULL PRIMARY KEY auto_increment,
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) NULL,
    message text NULL,
    created_date TIMESTAMP NULL,
    modified_date TIMESTAMP NULL,
    created_by VARCHAR(255) NULL,
    modified_by VARCHAR(255) NULL
);

INSERT INTO courses(title, short_description, long_description, image, course_date) values('Developing Computer Programs', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores.', 'course1.jpg', '2016-01-11 00:00:00');
INSERT INTO courses(title, short_description, long_description, image, course_date) values('Configuring Computer Hardware and Software', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. ', '', '2016-01-26 00:00:00');
INSERT INTO courses(title, short_description, long_description, image, course_date) values('Programming Apps for Mobile Devices', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores.', 'course3.jpg', '2016-02-08 00:00:00');

INSERT INTO introduction(title, description, image) values('Courses from Beginners to Advanced Learners', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio.</p></br></br><p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores.</p>', 'intro.jpg');

INSERT INTO contact(sitename, short_description, address, city, country, tel, email) values('CC Computer Courses', 'Welcome to my website', 'Cupertino, CA 95014', 'California', 'USA', '(408) 996–1010', 'contact@computercourses.com');