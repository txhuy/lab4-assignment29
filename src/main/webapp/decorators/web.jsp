<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/commons/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><dec:title default="Assignment 25" /></title>
<link href="<c:url value='/template/web/design.css' />" rel="stylesheet"
	type="text/css" media="all" />

<style type="text/css" media="all">
body {
    background-image: url(http://css.simplesite.com/images/v3/backgrounds/patterns/body/retro.png);
}
</style>
</head>
<body>
	<div class="container-fluid site-wrapper">	
		<%@ include file="/commons/web/header.jsp"%>
		<!-- this is the Content Wrapper -->
		<div class="container-fluid content-wrapper" id="content">
			<div class="container">
				<div class="row-fluid content-inner">
					<div id="left" class="span9">
						<!-- ADD "span12" if no sidebar, or "span9" with sidebar -->
						<div class="wrapper ">
							<div class="content">
								<dec:body />
							</div>
						</div>
					</div>
					<div id="right" class="span3">
						<%@ include file="/commons/web/menu.jsp"%>
					</div>
				</div>
			</div>
		</div>
		<!-- the controller has determined which view to be shown in the content -->
		<%@ include file="/commons/web/footer.jsp"%>
	</div>

	<script type="text/javascript"
		src="<c:url value='/template/web/frontendApp.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/template/web/analytics.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/template/web/gtm.js' />"></script>
</body>
</html>