<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglib.jsp"%>
<script src="<c:url value='/template/web/gen_validatorv4.js' />" type="text/javascript"></script>
<div class="wrapper contact">
	<div class="heading">
		<h1 class="page-title">Contact</h1>
	</div>

	<div class="content">
		<div class="section">
			<div class="content">
				<div class="span12 contact-text">
					<h4>${ contact.sitename }</h4>
					<p>
						Address: ${ contact.address }<br> City: ${ contact.city }<br>
						Country: ${ contact.country }
					</p>
					<p>
						Tel: ${ contact.tel }<br> Email: ${ contact.email }
					</p>
				</div>
			</div>
		</div>

		<div class="section contact-form-2">
			<div class="content">
				${ feedbackResult }
				<p>Write your message here. Fill out the form:</p>
				<form id="feedbackForm" action="contact" method="post">
					<fieldset>
						<div class="row-fluid">
							<div class="span6">
								<input type="text" name="username"
									placeholder="Write your name here"> <span
									class="help-block"></span>
							</div>
							<div class="span6">
								<input type="text" name="email"
									placeholder="Write your email here"> <span
									class="help-block"></span>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span12">
								<textarea name="message" placeholder="Write your message here"></textarea>
								<span class="help-block"></span>
							</div>
						</div>
						<div id="recaptcha" class="row-fluid"></div>
						<div class="row-fluid">
							<div class="span12">
								<button class="btn btn-primary pull-right" type="submit">Send
									- Click here</button>
							</div>
						</div>
					</fieldset>
				</form>

				<script type="text/javascript">
					var frmvalidator = new Validator("feedbackForm");
					frmvalidator.addValidation("username","req","Please enter your name");
					frmvalidator.addValidation("email","req","Please enter your email");
					frmvalidator.addValidation("email","email","Invalid email");
					frmvalidator.addValidation("message","req","Please enter your message");
				</script>
			</div>
		</div>
	</div>

</div>