<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/commons/taglib.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="section article">
	<div class="heading">
		<h3>${ introduction.title }</h3>
	</div>

	<div class="content">
		<div class="img-simple span6 pull-left">
			<div class="image">
				<a rel="nofollow" data-ss="imagemodal" data-href="res/${ introduction.image }">
					<img src="res/${ introduction.image }">
				</a>
			</div>
		</div>
		<div>${ introduction.description }</div>
	</div>
</div>

<div class="section news">
	<div class="heading">
		<h3>Program Overview</h3>
	</div>

	<div class="content">
		<dl class="dl-horizontal">
			<c:forEach items="${ courses.courses }" var="course">
				<dt>
					<span class="date-text"><fmt:formatDate pattern="dd-MMMM-yyyy" value="${ course.courseDate }" /></span>
				</dt>
				<dd>
					<h4>
						<a rel="nofollow" href="<c:url value='/courses'/>">${ course.title }</a>
					</h4>

					<p>${ course.shortDescription }</p>
				</dd>
			</c:forEach>
		</dl>
	</div>
</div>