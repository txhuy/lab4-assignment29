<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/commons/taglib.jsp"%>

<c:forEach var="course" items="${ courses.courses }">
	<div class="section article">
		<div class="heading">
			<h3>${ course.title }</h3>
		</div>

		<div class="content">
			<c:if test="${ not empty course.image }">
				<div class="img-simple span6 pull-left">
					<div class="image">
						<a rel="nofollow" data-ss="imagemodal"
							data-href="res/${ course.image }"> <img
							src="res/${ course.image }">
						</a>
					</div>
				</div>
			</c:if>
			<p>
				<span>${ course.longDescription }</span>
			</p>
		</div>
	</div>
</c:forEach>