<div class="container-fluid header-wrapper " id="header">
	<!-- this is the Header Wrapper -->
	<div class="container">
		<div class="title-wrapper">
			<div class="title-wrapper-inner">
				<div class="title ">${ sessionScope.sitename }</div>
				<div class="subtitle">${ sessionScope.shortDescription }</div>
			</div>
		</div>
		<!-- these are the titles -->
		<div class="navbar navbar-compact">
			<div class="navbar-inner">
				<div class="container">
					<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
					<a rel="nofollow" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse" title="Toggle menu"> <span
						class="menu-name">Menu</span> <span class="menu-bars"> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</span>
					</a>

					<!-- Everything you want hidden at 940px or less, place within here -->
					<div class="nav-collapse collapse">
						<ul class="nav" id="topMenu" data-submenu="horizontal">
							<li <c:if test="${ page == 'home' }">class=" active "</c:if> style=""><a rel="nofollow"
								href="<c:url value='/home'/>">Front Page</a></li>
							<li <c:if test="${ page == 'courses' }">class=" active "</c:if> style=""><a rel="nofollow"
								href="<c:url value='/courses'/>">Courses</a></li>
							<li <c:if test="${ page == 'contact' }">class=" active "</c:if> style=""><a rel="nofollow"
								href="<c:url value='/contact'/>">Contact</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- this is the Menu content -->
	</div>
	<!-- this is the Header content -->
</div>