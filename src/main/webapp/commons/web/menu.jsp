<div class="sidebar">
	<div class="wrapper share-box">
		<div class="heading">
			<h4>Share this page</h4>
		</div>

		<div class="content">
			<ul>
				<li><a id="share-facebook"
					href="http://us-123-machinery.simplesite.com/423587166#"> <i
						class="icon-facebook-sign"></i><span>Share on Facebook</span>
				</a></li>
				<li><a id="share-twitter"
					href="http://us-123-machinery.simplesite.com/423587166#"><i
						class="icon-twitter-sign"></i><span>Share on Twitter</span></a></li>
				<li><a id="share-google-plus"
					href="http://us-123-machinery.simplesite.com/423587166#"><i
						class="icon-google-plus-sign"></i><span>Share on Google+</span></a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper viral-box">
		<div class="heading">
			<h4>Create a website</h4>
		</div>

		<div class="content">
			<p>Everybody can create a website, it's easy.</p>
			<div class="bottom">
				<a rel="nofollow"
					href="http://www.simplesite.com/pages/receive.aspx?partnerkey=123i%3arightbanner&amp;referercustomerid=16328752&amp;refererpageid=423587166"
					class="btn btn-block">Try it for FREE now</a>
			</div>

		</div>
	</div>
</div>