<!-- this is the Footer Wrapper -->
<div class="container-fluid footer-wrapper" id="footer">
	<div class="container">
		<div class="footer-info">
			<div class="footer-info-text">This website was created with
				SimpleSite</div>
			<div class="footer-powered-by">
				<a rel="nofollow" href="http://www.simplesite.com/">Get Your own
					FREE website. Click here!</a>
			</div>
		</div>
		<div class="footer-page-counter" style="display: block; float: right;">
			
		</div>
		<div id="css_simplesite_com_fallback" class="hide"></div>
	</div>
</div>