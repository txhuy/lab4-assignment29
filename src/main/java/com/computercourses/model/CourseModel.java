package com.computercourses.model;

import java.sql.Timestamp;

public class CourseModel extends BaseModel {
	private String title;
	private String shortDescription;
	private String longDescription;
	private String image;
	private Timestamp courseDate;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getLongDescription() {
		return longDescription;
	}
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Timestamp getCourseDate() {
		return courseDate;
	}
	public void setCourseDate(Timestamp courseDate) {
		this.courseDate = courseDate;
	}
	
	
}
