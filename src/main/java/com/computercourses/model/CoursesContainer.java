package com.computercourses.model;

import java.util.List;

public class CoursesContainer {
	private List<CourseModel> courses;

	public List<CourseModel> getCourses() {
		return courses;
	}

	public void setCourses(List<CourseModel> courses) {
		this.courses = courses;
	}	
}