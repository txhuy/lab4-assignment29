package com.computercourses.dao;

import java.util.List;

import com.computercourses.model.CourseModel;

public interface ICoursesDAO {
	List<CourseModel> findAll();
}
