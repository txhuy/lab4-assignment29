package com.computercourses.dao.impl;

import java.util.List;

import com.computercourses.dao.IContactDAO;
import com.computercourses.mapper.ContactMapper;
import com.computercourses.model.ContactModel;
import com.computercourses.model.FeedbackModel;

public class ContactDAO extends BaseDAO<ContactModel> implements IContactDAO {
	
	@Override
	public boolean postFeedback(FeedbackModel feedback) {
		String sql = "INSERT INTO feedback(username, email, message) VALUES(?, ?, ?)";
		Long id = insert(sql, feedback.getUsername(), feedback.getEmail(), feedback.getMessage());
		return id != null;
	}

	@Override
	public ContactModel findContactInfo() {
		String sql = "SELECT * FROM contact";
		List<ContactModel> list = query(sql, new ContactMapper());
		if (!list.isEmpty()) {
			return list.get(0);
		} else {
			return null;
		}
	}

}
