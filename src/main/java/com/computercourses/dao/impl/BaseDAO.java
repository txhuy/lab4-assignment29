package com.computercourses.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.computercourses.dao.GenericDAO;
import com.computercourses.mapper.RowMapper;

public class BaseDAO<T> implements GenericDAO<T> {
	
	public Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/computercourses";
			String user = "root";
			String password = "1q2w3e4r!Q";
			return DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public <T> List<T> query(String sql, RowMapper<T> rowMapper, Object... parameters) {
		List<T> results = new ArrayList<>();
		Connection conn = getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		if (conn != null) {
			try {
				statement = conn.prepareStatement(sql);				
				setParameters(statement, parameters);				
				resultSet = statement.executeQuery();
				while(resultSet.next()) {					
					results.add(rowMapper.mapRow(resultSet));
				}
			} catch(SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					conn.close();
					if (statement != null) statement.close();
					if (resultSet != null) resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		}
		return results;
	}

	private void setParameters(PreparedStatement statement, Object[] parameters) throws SQLException {
		if (parameters.length > 0) {
			for (int i = 0; i < parameters.length; i++) {
				Object object = parameters[i];
				int paramIndex = i + 1;
				if (object instanceof Integer) {
					statement.setInt(paramIndex, (int) object);
				} else if (object instanceof String) {
					statement.setString(paramIndex, (String) object);
				}
			}
		}		
	}

	@Override
	public Long insert(String sql, Object... parameters) {
		Connection conn = getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Long id = null;
		if (conn != null) {
			try {
				conn.setAutoCommit(false);
				statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);				
				setParameters(statement, parameters);
				statement.executeUpdate();
				resultSet = statement.getGeneratedKeys();
				while(resultSet.next()) {					
					id = resultSet.getLong(1);
				}
				conn.commit();
			} catch(SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					conn.close();
					if (statement != null) statement.close();
					if (resultSet != null) resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		}
		return id;
	}
	
	

}
