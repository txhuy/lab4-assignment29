package com.computercourses.dao.impl;

import java.util.List;

import com.computercourses.dao.ICoursesDAO;
import com.computercourses.mapper.CourseMapper;
import com.computercourses.model.CourseModel;

public class CourseDAO extends BaseDAO<CourseModel> implements ICoursesDAO {

	@Override
	public List<CourseModel> findAll() {
		String sql = "SELECT * FROM courses";		
		return query(sql, new CourseMapper());
	}

}
