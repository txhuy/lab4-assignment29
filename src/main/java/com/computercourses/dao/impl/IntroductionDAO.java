package com.computercourses.dao.impl;

import java.util.List;

import com.computercourses.dao.IIntroductionDAO;
import com.computercourses.mapper.IntroductionMapper;
import com.computercourses.model.IntroductionModel;

public class IntroductionDAO extends BaseDAO<IntroductionModel> implements IIntroductionDAO {
	
	@Override
	public IntroductionModel findHomePageIntroduction() {		
		String sql = "SELECT * FROM introduction LIMIT 1";
		List<IntroductionModel> list = query(sql, new IntroductionMapper());
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
}
