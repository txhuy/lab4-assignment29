package com.computercourses.dao;

import com.computercourses.model.IntroductionModel;

public interface IIntroductionDAO {
	IntroductionModel findHomePageIntroduction();
}
 