package com.computercourses.dao;

import java.util.List;

import com.computercourses.mapper.RowMapper;


public interface GenericDAO<T> {
	<T> List<T> query(String sql, RowMapper<T> rowMapper, Object... parameters);
	Long insert(String sql, Object... parameters);
}
 