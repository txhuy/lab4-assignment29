package com.computercourses.dao;

import com.computercourses.model.ContactModel;
import com.computercourses.model.FeedbackModel;

public interface IContactDAO {
	ContactModel findContactInfo();
	boolean postFeedback(FeedbackModel feedback);
}
