package com.computercourses.service;

import com.computercourses.model.ContactModel;
import com.computercourses.model.FeedbackModel;

public interface IContactService {
	ContactModel findContactInfo();
	boolean sendFeedback(FeedbackModel feedbackModel);
}
