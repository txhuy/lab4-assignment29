package com.computercourses.service;

import com.computercourses.model.IntroductionModel;

public interface IIntroductionService {
	IntroductionModel findHomeIntroduction();
}
