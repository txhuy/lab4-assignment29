package com.computercourses.service.impl;

import javax.inject.Inject;

import com.computercourses.dao.IContactDAO;
import com.computercourses.model.ContactModel;
import com.computercourses.model.FeedbackModel;
import com.computercourses.service.IContactService;

public class ContactService implements IContactService {

	@Inject
	private IContactDAO contactDao;
	
	@Override
	public ContactModel findContactInfo() {
		return contactDao.findContactInfo();
	}

	@Override
	public boolean sendFeedback(FeedbackModel feedbackModel) {
		return contactDao.postFeedback(feedbackModel);		
	}

}