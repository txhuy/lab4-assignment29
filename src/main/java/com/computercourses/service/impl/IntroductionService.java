package com.computercourses.service.impl;

import javax.inject.Inject;

import com.computercourses.dao.IIntroductionDAO;
import com.computercourses.model.IntroductionModel;
import com.computercourses.service.IIntroductionService;

public class IntroductionService implements IIntroductionService {
	
	@Inject
	private IIntroductionDAO introductionDAO;

	@Override
	public IntroductionModel findHomeIntroduction() {
		return introductionDAO.findHomePageIntroduction();
	}
}
