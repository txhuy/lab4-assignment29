package com.computercourses.service.impl;

import javax.inject.Inject;

import com.computercourses.dao.ICoursesDAO;
import com.computercourses.model.CoursesContainer;
import com.computercourses.service.ICoursesService;

public class CoursesService implements ICoursesService {

	@Inject
	private ICoursesDAO coursesDao;
	
	@Override
	public CoursesContainer findAll() {
		CoursesContainer courses = new CoursesContainer();
		courses.setCourses(coursesDao.findAll());
		return courses;
	}

}
