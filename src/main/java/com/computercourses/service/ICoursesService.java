package com.computercourses.service;

import com.computercourses.model.CoursesContainer;

public interface ICoursesService {
	CoursesContainer findAll();
}
