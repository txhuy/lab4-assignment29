package com.computercourses.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.computercourses.model.FeedbackModel;
import com.computercourses.service.IContactService;

@WebServlet(urlPatterns = { "/contact" })
public class ContactController extends BaseController {

	private static final long serialVersionUID = 1L;

	@Inject
	private IContactService contactService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
		req.setAttribute("page", "contact");
		req.setAttribute("contact", contactService.findContactInfo());

		RequestDispatcher rd = req.getRequestDispatcher("/views/contact.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		String username = req.getParameter("username");
		String message = req.getParameter("message");
		String alert = null;

		FeedbackModel feedback = new FeedbackModel();
		feedback.setEmail(email);
		feedback.setUsername(username);
		feedback.setMessage(message);
		boolean sendFeedback = contactService.sendFeedback(feedback);

		String sendFeedbackSuccess = "<div class=\"alert alert-success\">Thank you for your message.</div>";
		String sendFeedbackFail = "<div class=\"alert alert-error\">Send feeback fail!</div>";

		alert = sendFeedback ? sendFeedbackSuccess : sendFeedbackFail;

		req.setAttribute("feedbackResult", alert);

		doGet(req, resp);
	}

}
