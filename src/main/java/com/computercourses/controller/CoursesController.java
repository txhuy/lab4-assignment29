package com.computercourses.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.computercourses.service.ICoursesService;

@WebServlet(urlPatterns = {"/courses"})
public class CoursesController extends BaseController {

	private static final long serialVersionUID = 1L;

	@Inject
	private ICoursesService coursesService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
		req.setAttribute("page", "courses");
		req.setAttribute("courses", coursesService.findAll());		
		
		RequestDispatcher rd = req.getRequestDispatcher("/views/courses.jsp");
		rd.forward(req, resp);
	}

}
