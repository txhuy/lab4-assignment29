package com.computercourses.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.computercourses.model.ContactModel;
import com.computercourses.service.IContactService;

public abstract class BaseController extends HttpServlet {
	
	@Inject
	protected IContactService contactService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();		
		if (session.getAttribute("sitename") == null || session.getAttribute("shortDescription") == null) {
			ContactModel contact = contactService.findContactInfo();
			session.setAttribute("sitename", contact.getSitename());
			session.setAttribute("shortDescription", contact.getShortDescription());
		}
	}
}
 