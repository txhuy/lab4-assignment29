package com.computercourses.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.computercourses.service.IIntroductionService;
import com.computercourses.service.ICoursesService;

@WebServlet(urlPatterns = {"/home"})
public class HomeController extends BaseController {
	private static final long serialVersionUID = -4937208918671503578L;
	
	@Inject
	private ICoursesService coursesService;
	@Inject
	private IIntroductionService introductionService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		super.doGet(req, resp);
		req.setAttribute("page", "home");
		
		req.setAttribute("introduction", introductionService.findHomeIntroduction());
		req.setAttribute("courses", coursesService.findAll());
		
		RequestDispatcher rd = req.getRequestDispatcher("/views/home.jsp");
		rd.forward(req, resp);
	}
}
