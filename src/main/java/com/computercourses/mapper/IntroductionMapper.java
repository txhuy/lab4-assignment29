package com.computercourses.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.computercourses.model.IntroductionModel;

public class IntroductionMapper implements RowMapper<IntroductionModel>{
	@Override
	public IntroductionModel mapRow(ResultSet resultSet) throws SQLException {
		IntroductionModel introductionModel = new IntroductionModel();
		introductionModel.setId(resultSet.getLong("id"));
		introductionModel.setTitle(resultSet.getString("title"));
		introductionModel.setDescription(resultSet.getString("description"));
		introductionModel.setImage(resultSet.getString("image"));
		return introductionModel;
	}
}
