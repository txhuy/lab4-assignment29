package com.computercourses.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.computercourses.model.CourseModel;

public class CourseMapper implements RowMapper<CourseModel> {

	@Override
	public CourseModel mapRow(ResultSet resultSet) throws SQLException {
		CourseModel course = new CourseModel();
		course.setId(resultSet.getLong("id"));
		course.setTitle(resultSet.getString("title"));
		course.setShortDescription(resultSet.getString("short_description"));
		course.setLongDescription(resultSet.getString("long_description"));
		course.setImage(resultSet.getString("image"));
		course.setCourseDate(resultSet.getTimestamp("course_date"));
		return course;
	}

}
