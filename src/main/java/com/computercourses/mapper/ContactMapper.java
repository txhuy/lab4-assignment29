package com.computercourses.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.computercourses.model.ContactModel;

public class ContactMapper implements RowMapper<ContactModel> {

	@Override
	public ContactModel mapRow(ResultSet resultSet) throws SQLException {
		ContactModel contact = new ContactModel();
		contact.setId(resultSet.getLong("id"));
		contact.setSitename(resultSet.getString("sitename"));
		contact.setShortDescription(resultSet.getString("short_description"));
		contact.setAddress(resultSet.getString("address"));
		contact.setCity(resultSet.getString("city"));
		contact.setCountry(resultSet.getString("country"));
		contact.setTel(resultSet.getString("tel"));
		contact.setEmail(resultSet.getString("email"));
		return contact;
	}

}
